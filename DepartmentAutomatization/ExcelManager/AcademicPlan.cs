﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelManager
{
    public class AcademicPlan
    {

        public AcademicPlan(string path)
        {
            Speciality = SpecialityInfoMatcher.GetData(path);
            Disciplines = DisciplineInfoMatcher.GetData(path);
        }

        public SpecialityInfo Speciality { get; set; }
        public List<DisciplineInfo> Disciplines { get; set; }


    }
}
