﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ExcelManager
{
    public static class SpecialityInfoMatcher
    {
        public static SpecialityInfo GetData(string path)
        {
            SpecialityInfo data = new SpecialityInfo();
            data.Title = GetTitle(Reader.GetRangeToList(path, "Speciality")[0]);
            data.Semesters=GetWeeksInSemester(Reader.GetRangeToList(path, "WeeksInSemester"));
            return data;
        }

        public static string GetTitle(string input)
        {
            StringBuilder title = new StringBuilder(Regex.Replace(input, @"\d", ""));
            title.Replace("Специальность", "");
            title.Replace("-", "");
            return Regex.Replace(title.ToString(), @"\s{2,}", "");
        }

       

        private static List<int> GetWeeksInSemester(List<string> weekSem)
        {
            List<int> WeeksInSemester = new List<int>();
            for(int i = 0; i < weekSem.Count; i++)
            {
                if (weekSem[i] != "0")
                {
                    int weeks = Convert.ToInt32(weekSem[i]);
                    WeeksInSemester.Add(weeks);
                }
            }
            return WeeksInSemester;
        }



    }
}
