﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ExcelManager
{
    public static class DisciplineInfoMatcher
    {

        private static List<string> names;
        private static List<List<string>> exams;
        private static List<List<string>> offsets;
        private static List<string> lections;
        private static List<string> lab;
        private static List<string> practicle;
        private static List<string> courseWorks;
        private static List<List<string>> supervisedIndependentWorks;
        private static List<string> weeksInSemester;
        private static List<List<string>> hoursInWeek;


        public static List<DisciplineInfo> GetData(string path)
        {
            ReadData(path);
            List<DisciplineInfo> data = new List<DisciplineInfo>();

            List<DisciplineInfo> matched = MatchAll();
            foreach(DisciplineInfo match in matched)
            {
                if (TaughtInOneCourse(match))
                {
                    data.Add(match);
                }
                else{
                   data.AddRange(SplitIntoCourses(match));
                }
            }
            return data;
        }



        public static void ReadData(string path)
        {
            names = Reader.GetFirstColumnFromRange(path, "DisciplineNames");
            exams = ParseTestList(Reader.GetFirstColumnFromRange(path, "Exams"));
            offsets = ParseTestList(Reader.GetFirstColumnFromRange(path, "Offsets"));
            lections = Reader.GetFirstColumnFromRange(path, "Lections");
            lab = Reader.GetFirstColumnFromRange(path, "Lab");
            practicle = Reader.GetFirstColumnFromRange(path, "Practicle");
            courseWorks = Reader.GetFirstColumnFromRange(path, "CourseWorks");
            supervisedIndependentWorks = StringEditor.DeleteNotesFromList(
                Reader.GetFirstColumnFromRange(path, "SupervisedIndependentWorks"));
            weeksInSemester = RemoveNulls(Reader.GetRangeToList(path, "WeeksInSemester"));
            hoursInWeek = SplintIntoLines(Reader.GetRangeToList(path, "HoursInWeek"), weeksInSemester.Count);
        }

        private static void AddIfUnique(List<DisciplineInfo> data, List<DisciplineInfo> list)
        {
            foreach (DisciplineInfo disciplineInfo in list)
            {
                if (!data.Contains(disciplineInfo))
                {
                    data.Add(disciplineInfo);
                }
            }
        }

        private static List<DisciplineInfo> CreateDisciplineInfoList(string[] testArray, int index)
        {
            List<DisciplineInfo> list = new List<DisciplineInfo>();
            for (int j = 0; j < testArray.Length; j++)
            {
                //DisciplineInfo temp = new DisciplineInfo(
                //    names[index],
                //    GetCourse(testArray[j]),
                //    lections[index],
                //    lab[index],
                //    practicle[index],
                //    courseWorks[index],
                //    supervisedIndependentWorks[index]);
                //list.Add(temp);
            }
            return list;
        }

        private static List<string> ParseTestString(string str)
        {
            string s = StringEditor.DeleteStar(str);
            string pattern = @"^\d{1,2}$";
            if (Regex.IsMatch(s, pattern))
            {
                return new List<string> { s };
            }
            else if (s.Contains(","))
            {
                return StringEditor.SplitByComma(s);
            }
            else if (s.Contains("-"))
            {
                return StringEditor.ParseByDash(s);
            }
            else
            {
                throw new ArgumentException("Invalid input!");
            }
        }

        


        public static string GetCourse(string semester)
        {
            if (semester == "0")
            {
                return semester;
            }
            double sem = Convert.ToDouble(semester);
            return Math.Ceiling(sem / 2).ToString();
        }

        private static bool IsDisciplineTitleValid(string title)
        {
            return (!title.Contains("Модуль")
                && !title.Contains("Факультативные")
                && !title.Contains("Дополнительные")
                && !title.Contains("Компонент")
                && !title.Contains("ЧАСТЬ"));
        }


        private static List<string> RemoveNulls(List<string> list)
        {
            List<string> noNulls = new List<string>();
            foreach (string item in list)
            {
                if (item != "0")
                {
                    noNulls.Add(item);
                }
            }
            return noNulls;
        }

        private static List<List<string>> ParseTestList(List<string> list)
        {
            List<List<string>> result = new List<List<string>>();
            foreach (string item in list)
            {
                result.Add(ParseTestString(item));
            }
            return result;
        }

        private static List<List<string>> SplintIntoLines(List<string> list, int semestersAmount)
        {
            List<List<string>> result = new List<List<string>>();
            List<string> temp = new List<string>();
            int columnsAmount = semestersAmount * 3;
            int counter = 0;

            for (int i = 0; i < list.Count; i += columnsAmount)
            {
                for (int j = i; j < i + columnsAmount; j++)
                {
                    // int index = counter * columnsAmount + j;
                    temp.Add(list[j]);
                }
                result.Add(new List<string>(temp));
                temp.Clear();
                counter++;
            }
            return result;
        }//list-часы в неделю, 
         //метод разбивает list на листы содержащие semestersAmount*3 колонок



        private static DisciplineInfo Match(int index)
        {
            return new DisciplineInfo(
                names[index], lections[index],
                exams[index], offsets[index], lab[index],
                practicle[index], courseWorks[index],
                supervisedIndependentWorks[index],
                hoursInWeek[index]
                );
        }
        public static List<DisciplineInfo> MatchAll()
        {
            List<DisciplineInfo> disciplines = new List<DisciplineInfo>();
            for (int i = 0; i < names.Count; i++)
            {
                if (IsDisciplineTitleValid(names[i]))
                {
                    disciplines.Add(Match(i));
                }
            }
            return disciplines;
        }

        private static bool TaughtInOneCourse(DisciplineInfo discipline)
        {
            return discipline.Courses.Count == 1;
        }

        private static List<DisciplineInfo> SplitIntoCourses(DisciplineInfo disciplineInfo)
        {
            List<DisciplineInfo> disciplines = new List<DisciplineInfo>();
            foreach (string course in disciplineInfo.Courses)
            {
                DisciplineInfo discipline = new DisciplineInfo();
                discipline.Title = disciplineInfo.Title;
                discipline.Courses.Add(course);
                int course_number = Convert.ToInt32(course);
                int weeksInFirstSemester = Convert.ToInt32(weeksInSemester[(course_number * 2) - 1]);
                int weeksInSecondSemester = Convert.ToInt32(weeksInSemester[course_number * 2]);

                List<string> hours = disciplineInfo.GetHoursByCourse(course_number,
                    weeksInFirstSemester, weeksInSecondSemester);
                   
                discipline.Lections = hours[0];
                discipline.Lab = hours[1];
                discipline.Practicle = hours[2];
                discipline.Exams = disciplineInfo.GetExamsByCourse(course_number);
                discipline.Offsets = disciplineInfo.GetOffsetsByCourse(course_number);
                discipline.CourseWorks = disciplineInfo.GetCourseWorksByCourse(course_number);
                discipline.SupervisedIndependentWorks = disciplineInfo.GetSIWByCourse(course_number);

                disciplines.Add(discipline);
            }
            return disciplines;
        }

    }
}
