﻿using ExcelManager.Info;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelManager.Matchers
{
    public class ContingentInfoMatcher
    {
        public static List<ContingentInfo> GetData(string path)
        {
            List<ContingentInfo> data = new List<ContingentInfo>();
            List<string> titles = Reader.GetRangeToList(path, "SpecialityTitles");
            List<string> firstCourse = Reader.GetRangeToList(path, "FirstCourse");
            List<string> secondCourse = Reader.GetRangeToList(path, "SecondCourse");
            List<string> thridCourse = Reader.GetRangeToList(path, "ThridCourse");
            List<string> fourthCourse = Reader.GetRangeToList(path, "FourthCourse");
            List<string> fifthCourse = Reader.GetRangeToList(path, "FifthCourse");
            int index = 0;
            foreach (string title in titles)
            {
                ContingentInfo contInfo = new ContingentInfo();
                contInfo.SpecialityTitle = title;
                contInfo.Courses.Add(1, FillCourse(firstCourse, index));
                contInfo.Courses.Add(2, FillCourse(secondCourse, index));
                contInfo.Courses.Add(3, FillCourse(thridCourse, index));
                contInfo.Courses.Add(4, FillCourse(fourthCourse, index));
                contInfo.Courses.Add(5, FillCourse(fifthCourse, index));
                data.Add(contInfo);
                index += 5;
            }
            return data;
        }

        private static CourseInfo FillCourse(List<string> course, int startIndex)
        {           
                return new CourseInfo
                {
                    Budget = Convert.ToInt32(course[startIndex]),
                    OffBudget = Convert.ToInt32(course[startIndex + 1]),
                    Foreing = Convert.ToInt32(course[startIndex + 2]),
                    BudgetGroups = Convert.ToDouble(course[startIndex + 3]),
                    OffBudgetGroups = Convert.ToDouble(course[startIndex + 4])
                };     
        }

    }
}
