﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Excel = Microsoft.Office.Interop.Excel;


namespace ExcelManager
{
    public static class Reader
    {
        public static List<string> GetFirstColumnFromRange(string workbookPath, string rangeName)
        {
            List<String> data = new List<string>();
            Excel.Application app = new Excel.Application();
            try
            {
                Excel.Workbook workbook = app.Workbooks.Open(workbookPath);
                Excel.Worksheet worksheet = workbook.Worksheets.get_Item(1);
                Excel.Range range = worksheet.Range[rangeName];
                int column = range.Columns.Count;
                for (int i = 1; i < range.Count; i += column)
                {
                    var value = range[i].Value;
                    if (value == null)
                    {
                        value = "0";
                    }
                    data.Add(value.ToString());
                }
                workbook.Close(false);
                app.Quit();
            }
            finally
            {
                CloseProcess();
            }
            return data;
        }

        public static List<string> GetRangeToList(string workbookPath, string rangeName)
        {
            List<String> data = new List<string>();
            Excel.Application app = new Excel.Application();
            try
            {
                Excel.Workbook workbook = app.Workbooks.Open(workbookPath);
                Excel.Worksheet worksheet = workbook.Worksheets.get_Item(1);
                Excel.Range range = worksheet.Range[rangeName];

                for (int i = 1; i < range.Count + 1; i++)
                {
                    var value = range[i].Value;
                    if (value == null)
                    {
                        value = "0";
                    }
                    data.Add(value.ToString());
                }
                workbook.Close(false);
                app.Quit();
            }
            finally
            {
                CloseProcess();
            }
            return data;
        }

        private static void CloseProcess()
        {
            Process[] List;
            List = Process.GetProcessesByName("EXCEL");
            foreach (Process proc in List)
            {
                proc.Kill();
            }
        }

        //public static List<string> GetRangeToList(string path, string rangeName)
        //{
        //    List<string> list = new List<string>();
        //    CXML.IXLWorkbook workbook = new CXML.XLWorkbook(path);
        //    CXML.IXLNamedRange namedRange = workbook.Worksheets.Worksheet(1).NamedRange(rangeName);
        //    CXML.IXLRanges ranges = namedRange.Ranges;
        //    var cells = ranges.Cells();
        //    foreach(CXML.IXLCell cell in cells)
        //    {
        //        list.Add(cell.Value.ToString());
        //    }
        //    return list;
        //}










    }
}
