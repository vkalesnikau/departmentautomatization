﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ExcelManager
{
    public static class StringEditor
    {
        public static List<string> SplitByComma(string str)
        {
            return str.Split(',').ToList();

        }
        public static List<List<string>> SplitListByComma(List<string> list)
        {
            List<List<string>> result = new List<List<string>>();
            foreach (string item in list)
            {
                result.Add(SplitByComma(item));
            }
            return result;
        }

        public static List<string> ParseByDash(string str)
        {
            int index = str.IndexOf("-");
            int first = Convert.ToInt32(str.Substring(0, index));
            int last = Convert.ToInt32(str.Substring(index + 1));
            string[] array = new string[last - first + 1];
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = first.ToString();
                first++;
            }
            return array.ToList();
        }

        public static string DeleteStar(string str)
        {
            string newStr = str.Replace("*", "");
            return newStr;
        }

        public static string DeleteRepeatableSpaces(string input)
        {
            string newStr = Regex.Replace(input, @"\s+", "");
            return newStr;
        }
        public static string ListToString(List<string> list)
        {
            string result = "";
            foreach (string str in list)
            {
                result += str + ",";
            }
            if (result != "")
            {
                return result.Substring(0, result.Length - 1);

            }
            else
            {
                return result;
            }
        }

        public static List<string> DeleteNotes(string str)
        {
            List<string> list = str.Split(',').ToList();
            List<string> result = new List<string>();
            foreach (string item in list)
            {
                if (item.Contains("к"))
                {
                    int kindex = item.IndexOf("к");
                    result.Add(item.Substring(0, kindex + 1));
                }
                else if (item.Contains("ф"))
                {
                    int findex = item.IndexOf("ф");
                    result.Add(item.Substring(0, findex + 1));
                }
                else
                {
                    result.Add(item);
                }
            }
            return result;

        }

        public static List<List<string>> DeleteNotesFromList(List<string> list)
        {
            List<List<string>> result = new List<List<string>>();
            foreach (string item in list)
            {
                result.Add(DeleteNotes(item));
            }
            return result;
        }
    }
}
