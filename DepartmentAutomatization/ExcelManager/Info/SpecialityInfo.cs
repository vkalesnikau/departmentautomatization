﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelManager
{
    public class SpecialityInfo
    {
        public string Title { get; set; }
        public List<int> Semesters { get; set; }
        

    }
}
