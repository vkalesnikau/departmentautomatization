﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelManager.Info
{
    public class ContingentInfo
    {
        public ContingentInfo()
        {
            Courses = new Dictionary<int, CourseInfo>();
        }

        public Dictionary<int, CourseInfo> Courses;
        public string SpecialityTitle { get; set; }


    }
}
