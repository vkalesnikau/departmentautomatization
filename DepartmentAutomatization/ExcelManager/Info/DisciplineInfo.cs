﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelManager
{
    public class DisciplineInfo
    {
        public DisciplineInfo()
        {
            Exams = new List<string>();
            Offsets = new List<string>();
            HoursInWeek = new List<string>();
            SupervisedIndependentWorks = new List<string>();
        }

        public DisciplineInfo(string title, string lections,
            List<string> exams, List<string> offsets,
            string lab, string practicle, string courseWorks,
            List<string> supervisedIndependentWorks, List<string> hoursInWeek)
        {
            Title = title;
            Exams = exams;
            Offsets = offsets;
            Lections = lections;
            Lab = lab;
            Practicle = practicle;
            CourseWorks = courseWorks;
            SupervisedIndependentWorks = supervisedIndependentWorks;
            HoursInWeek = hoursInWeek;

        }

        public string Title { get; set; }
        public string Lections { get; set; }
        public string Lab { get; set; }
        public string Practicle { get; set; }
        public string CourseWorks { get; set; }
        public List<string> SupervisedIndependentWorks { get; set; }
        public List<string> Exams;
        public List<string> Offsets;
        public List<string> HoursInWeek { get; set; }


        public List<string> Courses
        {
            get
            {
                List<string> courses = new List<string>();
                AddIfUnique(courses, GetCoursesFromTests(Exams));
                AddIfUnique(courses, GetCoursesFromTests(Offsets));
                return courses;
            }
        }

        private static void AddIfUnique(List<string> destination, List<string> source)
        {
            foreach (string item in source)
            {
                if (!destination.Contains(item))
                {
                    destination.Add(item);
                }
            }
        }

        private static List<string> GetCoursesFromTests(List<string> tests)
        {
            List<string> courses = new List<string>();
            if (tests.Count != 0)
            {
                foreach (string item in tests)
                {
                    if (item != "0")
                    {
                        courses.Add(GetCourse(item));
                    }
                }
            }
            return courses;
        }

        private static string GetCourse(string semester)
        {
            if (semester == "0")
            {
                return semester;
            }
            double sem = Convert.ToDouble(semester);
            return Math.Ceiling(sem / 2).ToString();
        }

        public List<string> GetHoursByCourse(int course, int weeksInFirstSemester,
            int weeksInSecondSemester)
        {
            List<string> hours = new List<string>();
            int index = (course * 6) - 1;
            for (int i = index; i > index - 3; i--)
            {
                double hoursInFirst = Convert.ToDouble(HoursInWeek[i - 3]);
                double hoursInSecond = Convert.ToDouble(HoursInWeek[i]);
                hours.Add((hoursInFirst * weeksInFirstSemester + hoursInSecond * weeksInSecondSemester).ToString());
            }
            hours.Reverse();
            return hours;

        }//!!!!

        public List<string> GetExamsByCourse(int course) // одинаковый код
        {
            List<string> exams = new List<string>();
            foreach (string exam in Exams)
            {
                int ex = Convert.ToInt32(exam);
                if (ex == course * 2 || ex == ((course * 2) - 1))
                {
                    exams.Add(ex.ToString());
                }
            }
            return exams;
        }
        public List<string> GetOffsetsByCourse(int course)
        {
            List<string> offsets = new List<string>();
            foreach (string offset in Offsets)
            {
                int ofs = Convert.ToInt32(offset);
                if (ofs == course * 2 || ofs == ((course * 2) - 1))
                {
                    offsets.Add(ofs.ToString());
                }
            }
            return offsets;
        }
        public string GetCourseWorksByCourse(int course)
        {
            int cw = Convert.ToInt32(CourseWorks);
            if (cw == course * 2 || cw == ((course * 2) - 1))
            {
                return cw.ToString();
            }
            else
            {
                return "0";
            }
        }


        public List<string> GetSIWByCourse(int course)
        {
            List<string> siw = new List<string>();
            foreach (string work in SupervisedIndependentWorks)
            {
                int first = (course * 2) - 1;
                int second = course * 2;
                if (work.Contains(first.ToString())
                    || work.Contains(second.ToString()))
                {
                    siw.Add(work);
                }
            }
            return siw;
        }


        //public override bool Equals(object obj)
        //{
        //    DisciplineInfo other = (DisciplineInfo)obj;
        //    return (Title == other.Title && Course == other.Course);
        //}
        //public override int GetHashCode()
        //{
        //    return Title.GetHashCode() + Course.GetHashCode();
        //}


    }
}
