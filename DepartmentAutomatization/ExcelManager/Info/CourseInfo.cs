﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelManager.Info
{
    public class CourseInfo
    {
        public int Budget { get; set; }
        public int OffBudget { get; set; }
        public int Foreing { get; set; }
        public double BudgetGroups { get; set; }
        public double OffBudgetGroups { get; set; }
        public int StudentsAmount {
            get
            {
                return Budget + OffBudget + Foreing;
            }
        }

        public double GroupsAmount
        {
            get { return BudgetGroups + OffBudgetGroups; }
        }

        public override string ToString()
        {
            string s = "Budget " + Budget+ "OffBudget " + OffBudget+
                "Foreing " + Foreing + "BudgetGroups " + BudgetGroups + "OffBudgetGroups " + OffBudgetGroups;
            return s;
        }


    }
}
