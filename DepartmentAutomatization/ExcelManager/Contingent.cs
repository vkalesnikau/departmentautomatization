﻿using ExcelManager.Info;
using ExcelManager.Matchers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelManager
{
    public class Contingent
    {
        public Contingent(string path)
        {
            Specialities = ContingentInfoMatcher.GetData(path);
        }

        public List<ContingentInfo> Specialities;

        public ContingentInfo GetContingentInfoBySpecialityTitle(string Title)
        {
            
            foreach(ContingentInfo ci in Specialities)
            {
                if(ci.SpecialityTitle == Title)
                {
                    return ci;
                }
            }
            return null;
        }
    }
}
