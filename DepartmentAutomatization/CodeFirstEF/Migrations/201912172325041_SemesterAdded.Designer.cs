// <auto-generated />
namespace CodeFirstEF.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class SemesterAdded : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(SemesterAdded));
        
        string IMigrationMetadata.Id
        {
            get { return "201912172325041_SemesterAdded"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
