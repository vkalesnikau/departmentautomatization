namespace CodeFirstEF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Disciplines",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Course = c.Int(nullable: false),
                        BudgetStudents = c.Int(nullable: false),
                        OffBudgetStudents = c.Int(nullable: false),
                        BudgetGroups = c.Double(nullable: false),
                        OffBudgetGroups = c.Double(nullable: false),
                        LectionHours = c.Int(nullable: false),
                        LabHours = c.Int(nullable: false),
                        PracticleHours = c.Int(nullable: false),
                        Exam = c.Boolean(nullable: false),
                        Offset = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Specialities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Semesters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Number = c.Int(nullable: false),
                        Duration = c.Int(nullable: false),
                        Speciality_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Specialities", t => t.Speciality_Id)
                .Index(t => t.Speciality_Id);
            
            CreateTable(
                "dbo.SpecialityDisciplines",
                c => new
                    {
                        Speciality_Id = c.Int(nullable: false),
                        Discipline_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Speciality_Id, t.Discipline_Id })
                .ForeignKey("dbo.Specialities", t => t.Speciality_Id, cascadeDelete: true)
                .ForeignKey("dbo.Disciplines", t => t.Discipline_Id, cascadeDelete: true)
                .Index(t => t.Speciality_Id)
                .Index(t => t.Discipline_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Semesters", "Speciality_Id", "dbo.Specialities");
            DropForeignKey("dbo.SpecialityDisciplines", "Discipline_Id", "dbo.Disciplines");
            DropForeignKey("dbo.SpecialityDisciplines", "Speciality_Id", "dbo.Specialities");
            DropIndex("dbo.SpecialityDisciplines", new[] { "Discipline_Id" });
            DropIndex("dbo.SpecialityDisciplines", new[] { "Speciality_Id" });
            DropIndex("dbo.Semesters", new[] { "Speciality_Id" });
            DropTable("dbo.SpecialityDisciplines");
            DropTable("dbo.Semesters");
            DropTable("dbo.Specialities");
            DropTable("dbo.Disciplines");
        }
    }
}
