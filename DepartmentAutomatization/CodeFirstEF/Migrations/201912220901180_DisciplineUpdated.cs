namespace CodeFirstEF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DisciplineUpdated : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Disciplines", "Exams", c => c.String());
            AddColumn("dbo.Disciplines", "Offsets", c => c.String());
            DropColumn("dbo.Disciplines", "Exam");
            DropColumn("dbo.Disciplines", "Offset");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Disciplines", "Offset", c => c.Boolean(nullable: false));
            AddColumn("dbo.Disciplines", "Exam", c => c.Boolean(nullable: false));
            DropColumn("dbo.Disciplines", "Offsets");
            DropColumn("dbo.Disciplines", "Exams");
        }
    }
}
