namespace CodeFirstEF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CourseWorksAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Disciplines", "CourseWorks", c => c.Int(nullable: false));
            AddColumn("dbo.Disciplines", "SupervisedIndependentWorks", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Disciplines", "SupervisedIndependentWorks");
            DropColumn("dbo.Disciplines", "CourseWorks");
        }
    }
}
