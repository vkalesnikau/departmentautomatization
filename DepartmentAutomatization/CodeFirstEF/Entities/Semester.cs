﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstEF.Entities
{
    public class Semester
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public int Duration { get; set; }
    }
}
