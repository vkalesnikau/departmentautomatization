﻿using ExcelManager;
using ExcelManager.Info;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstEF
{

    public class Discipline
    {
        public Discipline()
        {
            this.Specialities = new List<Speciality>();
        }
        public Discipline(DisciplineInfo disciplineInfo, ContingentInfo contingentInfo)
        {
            int courseNumber = Convert.ToInt32(disciplineInfo.Courses[0]);
            Title = disciplineInfo.Title;
            Course = courseNumber;
            BudgetStudents = contingentInfo.Courses[courseNumber].Budget;
            OffBudgetStudents = contingentInfo.Courses[courseNumber].OffBudget;
            BudgetGroups = contingentInfo.Courses[courseNumber].BudgetGroups;
            OffBudgetGroups = contingentInfo.Courses[courseNumber].OffBudgetGroups;
            LectionHours = Convert.ToInt32(disciplineInfo.Lections);
            LabHours = Convert.ToInt32(disciplineInfo.Lab);
            PracticleHours = Convert.ToInt32(disciplineInfo.Practicle);
            CourseWorks = Convert.ToInt32(disciplineInfo.CourseWorks);
            SupervisedIndependentWorks = StringEditor.ListToString(disciplineInfo.SupervisedIndependentWorks);
            Exams = StringEditor.ListToString(disciplineInfo.Exams);
            Offsets = StringEditor.ListToString(disciplineInfo.Offsets);
            Specialities = new List<Speciality>();

        }


        public int Id { get; set; }
        public string Title { get; set; }
        public int Course { get; set; }
        public int BudgetStudents { get; set; }
        public int OffBudgetStudents { get; set; }
        public double BudgetGroups { get; set; }
        public double OffBudgetGroups { get; set; }
        public int LectionHours { get; set; }
        public int LabHours { get; set; }
        public int PracticleHours { get; set; }
        public int CourseWorks { get; set; }
        public string SupervisedIndependentWorks { get; set; }
        public string Exams { get; set; }
        public string Offsets { get; set; }


        public ICollection<Speciality> Specialities { get; set; }

        public override string ToString()
        {
            return Title + " " + Course + " курс";
        }
    }
}
