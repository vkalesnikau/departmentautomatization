﻿using System.Collections.Generic;
using ExcelManager;
using CodeFirstEF.Entities;

namespace CodeFirstEF
{
    public class Speciality
    {
        private SpecialityInfo speciality;

        public Speciality()
        {
            this.Disciplines = new List<Discipline>();
            Semesters = new List<Semester>();
        }

        public Speciality(SpecialityInfo speciality)
        {
            Title = speciality.Title;
            Disciplines = new List<Discipline>();
            Semesters = new List<Semester>();
            int number = 1;
            foreach(int weeksAmount in speciality.Semesters)
            {
                this.Semesters.Add(new Semester
                {
                    Number = number,
                    Duration = weeksAmount
                });
                number++;
            }
        }

        public int Id { get; set; }
        public string Title { get; set; }

        public virtual List<Semester> Semesters { get; set; }        
        public ICollection<Discipline> Disciplines { get; set; }
    }
}