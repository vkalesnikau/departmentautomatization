﻿using CodeFirstEF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstEF.Repositories
{
    public class SpecialityRepository : Repository<Speciality>
    {
        public override void Delete(int Id)
        {
            using (database = new DepartmentAutomatizationModel())
            {
                Speciality speciality = database.Specialities.Find(Id);
                database.Specialities.Remove(speciality);
                database.SaveChanges(); 
            }
        }

        public override Speciality GetItem(int Id)
        {
            using (database = new DepartmentAutomatizationModel())
            {
                return database.Specialities
                    .Include(s => s.Semesters)
                    .Where(s => s.Id == Id).First();
                //return database.Specialities.Find(Id);
            }
        }

        public override void DeleteAll()
        {
            
            using (database = new DepartmentAutomatizationModel())
            {
                List<Speciality> all = database.Specialities.ToList();
                foreach(Speciality speciality in all)
                {
                    database.Specialities.Remove(speciality);
                }
                database.SaveChanges();
            }
        }

        public override void Insert(Speciality item)
        {
            using (database = new DepartmentAutomatizationModel())
            {
                database.Specialities.Add(item);
                database.SaveChanges();
            }
        }

        public override void Update(int Id, Speciality item)
        {
            using (database = new DepartmentAutomatizationModel())
            {
                Speciality speciality = database.Specialities.Find(Id);
                speciality.Title = item.Title;
                database.SaveChanges();
            }
        }
    }
}