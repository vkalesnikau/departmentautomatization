﻿
using CodeFirstEF;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstEF.Repositories
{
    public class DisciplineRepository : Repository<Discipline>
    {
        public override void Delete(int Id)
        {
            using (database = new DepartmentAutomatizationModel())
            {
                Discipline discipline = database.Disciplines.Find(Id);
                if (discipline != null)
                {
                    database.Disciplines.Remove(discipline);

                }
                database.SaveChanges();
            }
        }

        public override void DeleteAll()
        {
            using (database = new DepartmentAutomatizationModel())
            {
                List<Discipline> all = database.Disciplines.ToList();
                foreach (Discipline discipline in all)
                {
                    database.Disciplines.Remove(discipline);
                }
                database.SaveChanges();
            }
        }
        public List<Discipline> GetAll()
        {
            using (database = new DepartmentAutomatizationModel())
            {
                List<Discipline> all = database.Disciplines.ToList();
                return all;
                
            }
        }

        public override Discipline GetItem(int Id)
        {
            using (database = new DepartmentAutomatizationModel())
            {
                return database.Disciplines.Find(Id);
            }
        }

        public override void Insert(Discipline item)
        {
            using (database = new DepartmentAutomatizationModel())
            {
                Discipline discipline = database.Disciplines.Add(item);
                database.SaveChanges();
            }
        }

        public override void Update(int Id, Discipline item)
        {
            using (database = new DepartmentAutomatizationModel())
            {
                Discipline discipline = database.Disciplines.Find(Id);
                discipline.Title = item.Title;
                discipline.Course = item.Course;
                discipline.BudgetStudents = item.BudgetStudents;
                discipline.OffBudgetStudents = item.OffBudgetStudents;
                discipline.BudgetGroups = item.BudgetGroups;
                discipline.OffBudgetGroups = item.OffBudgetGroups;
                discipline.CourseWorks = item.CourseWorks;
                discipline.LabHours = item.LabHours;
                discipline.LectionHours = item.LectionHours;
                discipline.PracticleHours = item.PracticleHours;
                discipline.SupervisedIndependentWorks = item.SupervisedIndependentWorks;
                discipline.Exams = item.Exams;
                discipline.Offsets = item.Offsets;

                database.SaveChanges();
            }
        }
    }
}
