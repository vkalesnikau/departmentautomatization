﻿using CodeFirstEF.Entities;
using CodeFirstEF.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstEF.Repositories
{
    public class SemesterRepository : Repository<Semester>
    {
        public override void Delete(int Id)
        {
            using (database = new DepartmentAutomatizationModel())
            {
                Semester semester = database.Semesters.Find(Id);
                database.Semesters.Remove(semester);
                database.SaveChanges();
            }
        }

        public override Semester GetItem(int Id)
        {
            using (database = new DepartmentAutomatizationModel())
            {
                return database.Semesters.Find(Id);
            }
        }

        public override void DeleteAll()
        {

            using (database = new DepartmentAutomatizationModel())
            {
                List<Semester> all = database.Semesters.ToList();
                foreach (Semester semester in all)
                {
                    database.Semesters.Remove(semester);
                }
                database.SaveChanges();
            }
        }

        public override void Insert(Semester item)
        {
            using (database = new DepartmentAutomatizationModel())
            {
                database.Semesters.Add(item);
                database.SaveChanges();
            }
        }

        public override void Update(int Id, Semester item)
        {
            using (database = new DepartmentAutomatizationModel())
            {
                Semester semester = database.Semesters.Find(Id);
                semester.Number = item.Number;
                semester.Duration = item.Duration;
                database.SaveChanges();
            }
        }
    }
}
