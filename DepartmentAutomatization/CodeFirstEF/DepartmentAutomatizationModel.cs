﻿namespace CodeFirstEF
{
    using Entities;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class DepartmentAutomatizationModel : DbContext
    {
        // Контекст настроен для использования строки подключения "DepartmentAutomatizationModel" из файла конфигурации  
        // приложения (App.config или Web.config). По умолчанию эта строка подключения указывает на базу данных 
        // "CodeFirstEF.DepartmentAutomatizationModel" в экземпляре LocalDb. 
        // 
        // Если требуется выбрать другую базу данных или поставщик базы данных, измените строку подключения "DepartmentAutomatizationModel" 
        // в файле конфигурации приложения.
        public DepartmentAutomatizationModel()
            : base("name=DepartmentAutomatizationModel")
        {
        }

        // Добавьте DbSet для каждого типа сущности, который требуется включить в модель. Дополнительные сведения 
        // о настройке и использовании модели Code First см. в статье http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<Speciality> Specialities { get; set; }
        public virtual DbSet<Discipline> Disciplines { get; set; }
        public virtual DbSet<Semester> Semesters { get; set; }


    }

}