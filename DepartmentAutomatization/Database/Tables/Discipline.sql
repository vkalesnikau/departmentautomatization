﻿CREATE TABLE [dbo].[Discipline]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Title] NVARCHAR(100) NOT NULL, 
    [Course] INT NULL, 
    [BudgetStudentsAmount] INT NULL, 
	[OffBudgetStudentsAmount] INT NULL,    
    [BudgetGroupsAmount] FLOAT NULL, 
	[OffBudgetGroupsAmount] FLOAT NULL,
	[StreamsAmount] INT NULL, 
    [SubgroupAmount] INT NULL, 
    [LectionHours] INT NULL, 
    [LabHours] INT NULL, 
    [PracticleHours] INT NULL, 
    [IndividualTasksAmount] INT NULL, 
    [ExamsAmount] INT NULL, 
    [OffsetsAmount] INT NULL, 
    [CourseProjectsAmount] INT NULL, 
    [CourseWorksAmount] INT NULL, 
    [SeminarHours] INT NULL, 
    [AllHours] INT NULL, 
    
)
