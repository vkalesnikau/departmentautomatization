﻿CREATE TABLE [dbo].[SpecpialityDiscipline]( 
	[SpecialityId] INT REFERENCES Speciality (Id) NOT NULL,
	[DisciplineId] INT REFERENCES Discipline (Id) NOT NULL
)
