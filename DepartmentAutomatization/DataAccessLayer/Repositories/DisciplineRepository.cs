﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories
{
    public class DisciplineRepository : Repository<Discipline>
    {
        public override void Delete(int Id)
        {
            using (database = new DepartmentAutomatizationEntities())
            {
                Discipline discipline = database.Disciplines.Find(Id);
                if (discipline != null)
                {
                    database.Disciplines.Remove(discipline);

                }
                database.SaveChanges();
            }
        }

        public override Discipline GetItem(int Id)
        {
            using (database = new DepartmentAutomatizationEntities())
            {
                return database.Disciplines.Find(Id);
            }
        }

        public override void Insert(Discipline item)
        {
            using (database = new DepartmentAutomatizationEntities())
            {
                Discipline discipline = database.Disciplines.Add(item);
                database.SaveChanges();

            }
        }

        public override void Update(int Id, Discipline item)
        {
            using (database = new DepartmentAutomatizationEntities())
            {
                Discipline discipline = database.Disciplines.Find(Id);
                discipline.Title = item.Title;
                discipline.Course = item.Course;
                discipline.BudgetStudentsAmount = item.BudgetStudentsAmount;
                discipline.OffBudgetStudentsAmount = item.OffBudgetStudentsAmount;
                discipline.StreamsAmount = item.StreamsAmount;
                discipline.BudgetGroupsAmount = item.BudgetGroupsAmount;
                discipline.OffBudgetGroupsAmount = item.OffBudgetGroupsAmount;
                discipline.SubgroupAmount = item.SubgroupAmount;
                discipline.LectionHours = item.LectionHours;
                discipline.LabHours = item.LabHours;
                discipline.PracticleHours = item.PracticleHours;
                discipline.IndividualTasksAmount = item.IndividualTasksAmount;
                discipline.ExamsAmount = item.ExamsAmount;
                discipline.OffsetsAmount = item.OffsetsAmount;
                discipline.CourseProjectsAmount = item.CourseProjectsAmount;
                discipline.CourseWorksAmount = item.CourseWorksAmount;
                discipline.SeminarHours = item.SeminarHours;
                discipline.AllHours = item.AllHours;
                database.SaveChanges();
            }
        }

        public void Insert(Entities.Discipline discipline)
        {
            throw new NotImplementedException();
        }
    }
}
