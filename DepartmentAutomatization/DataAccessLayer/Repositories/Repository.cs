﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories
{
    public abstract class Repository<T>
    {
        public DepartmentAutomatizationEntities database;
        public abstract void Insert(T item);
        public abstract T GetItem(int Id);
        public abstract void Delete(int Id);
        public abstract void Update(int Id, T item);

    }
}