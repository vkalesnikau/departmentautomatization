﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories
{
    public class SpecialityRepository : Repository<Speciality>
    {
        public override void Delete(int Id)
        { 
            using (database = new DepartmentAutomatizationEntities())
            {
                Speciality speciality = database.Specialities.Find(Id);
                database.Specialities.Remove(speciality);
                database.SaveChanges();
            }
        }

        public override Speciality GetItem(int Id)
        {
            using (database = new DepartmentAutomatizationEntities())
            {
                return database.Specialities.Find(Id);
            }
        }

        public override void Insert(Speciality item)
        {
            using (database = new DepartmentAutomatizationEntities())
            {
                database.Specialities.Add(item);
                database.SaveChanges();
            }
        }

        public override void Update(int Id, Speciality item)
        {
            using (database = new DepartmentAutomatizationEntities())
            {
                Speciality speciality = database.Specialities.Find(Id);
                speciality.Title = item.Title;
                database.SaveChanges();
            }
        }
    }
}