﻿using ExcelManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities
{
    public class Speciality
    {
        public Speciality()
        {
            this.Disciplines = new List<Discipline>();
        }

        public int Id { get; set; }
        public string Title { get; set; }

        public  ICollection<Discipline> Disciplines { get; set; }

    
        // public Speciality (AcademicPlan plan)
        //{
        //    Title = plan.Speciality.Title;
        //    foreach(DisciplineInfo discipline in plan.Disciplines)
        //    {
        //        Discipline.Add(new Entities.Discipline(discipline))
        //    }
        //}



    }
}
