﻿using ExcelManager;
using ExcelManager.Info;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DataAccessLayer.Entities
{
    public class Discipline
    {
        public Discipline()
        {
            this.Specialities = new List<Speciality>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public Nullable<int> Course { get; set; }
        public Nullable<int> BudgetStudentsAmount { get; set; }
        public Nullable<int> OffBudgetStudentsAmount { get; set; }
        public Nullable<double> BudgetGroupsAmount { get; set; }
        public Nullable<double> OffBudgetGroupsAmount { get; set; }
        public Nullable<int> StreamsAmount { get; set; }
        public Nullable<int> SubgroupAmount { get; set; }
        public Nullable<int> LectionHours { get; set; }
        public Nullable<int> LabHours { get; set; }
        public Nullable<int> PracticleHours { get; set; }
        public Nullable<int> IndividualTasksAmount { get; set; }
        public Nullable<int> ExamsAmount { get; set; }
        public Nullable<int> OffsetsAmount { get; set; }
        public Nullable<int> CourseProjectsAmount { get; set; }
        public Nullable<int> CourseWorksAmount { get; set; }
        public Nullable<int> SeminarHours { get; set; }
        public Nullable<int> AllHours { get; set; }

        public  ICollection<Speciality> Specialities { get; set; }
       
    }
}

