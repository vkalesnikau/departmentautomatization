//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccessLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class Discipline
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Discipline()
        {
            this.Specialities = new HashSet<Speciality>();
        }
    
        public int Id { get; set; }
        public string Title { get; set; }
        public Nullable<int> Course { get; set; }
        public Nullable<int> BudgetStudentsAmount { get; set; }
        public Nullable<int> OffBudgetStudentsAmount { get; set; }
        public Nullable<double> BudgetGroupsAmount { get; set; }
        public Nullable<double> OffBudgetGroupsAmount { get; set; }
        public Nullable<int> StreamsAmount { get; set; }
        public Nullable<int> SubgroupAmount { get; set; }
        public Nullable<int> LectionHours { get; set; }
        public Nullable<int> LabHours { get; set; }
        public Nullable<int> PracticleHours { get; set; }
        public Nullable<int> IndividualTasksAmount { get; set; }
        public Nullable<int> ExamsAmount { get; set; }
        public Nullable<int> OffsetsAmount { get; set; }
        public Nullable<int> CourseProjectsAmount { get; set; }
        public Nullable<int> CourseWorksAmount { get; set; }
        public Nullable<int> SeminarHours { get; set; }
        public Nullable<int> AllHours { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Speciality> Specialities { get; set; }
    }
}
