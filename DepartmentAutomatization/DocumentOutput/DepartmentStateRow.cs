﻿using CodeFirstEF;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace DocumentOutput
{
    public class DepartmentStateRow
    {
        public Discipline discipline;

        private const double MODULE_RATING = 0.1;
        private const int COURSE_PROJECTS = 3;
        private const double TEST_REVIEW = 0.75;
        private const double OFFSET = 0.35;
        private const double EXAM = 0.5;


        public DepartmentStateRow(Discipline discipline)
        {
            Streams = 1;
            Subgroups = 2;
            this.discipline = discipline;
        }

        public int Streams { get; set; }
        public int Subgroups { get; private set; }

        public double ModuleRatingSystemBudget
        {
            get { return MODULE_RATING * discipline.BudgetStudents; }
        }
        public double ModuleRatingSystemOffBudget
        {
            get { return MODULE_RATING * discipline.OffBudgetStudents; }
        }
        public int CourseProjectsBudget
        {
            get
            {
                if (discipline.CourseWorks != 0)
                {
                    return COURSE_PROJECTS * discipline.BudgetStudents;
                }
                else
                {
                    return 0;
                }
            }
        }
        public int CourseProjectsOffBudget
        {
            get
            {
                if (discipline.CourseWorks != 0)
                {
                    return COURSE_PROJECTS * discipline.OffBudgetStudents;
                }
                else
                {
                    return 0;
                }
            }

        }
        public double ConsultationBudget
        {
            get
            {
                return (discipline.LectionHours * 0.05 + 2) * discipline.BudgetGroups;
            }
        }
        public double ConsultationOffBudget
        {
            get
            {
                return (discipline.LectionHours * 0.05 + 2) * discipline.OffBudgetGroups;
            }
        }

        public double TestReviewBudget
        {
            get
            {
                if (discipline.SupervisedIndependentWorks.Contains("к"))
                {
                    return TEST_REVIEW * discipline.BudgetStudents;
                }
                else
                {
                    return 0;
                }
            }
        }
        public double TestReviewOffBudget
        {
            get
            {
                if (discipline.SupervisedIndependentWorks.Contains("к"))
                {
                    return TEST_REVIEW * discipline.OffBudgetStudents;
                }
                else
                {
                    return 0;
                }
            }
        }
        public double OffsetsBudget
        {
            get
            {
                return OFFSET * discipline.BudgetStudents;
            }
        }
        public double OffsetsOffBudget
        {
            get
            {
                return OFFSET * discipline.OffBudgetStudents;
            }
        }
        public double ExamsBudget
        {
            get
            {
                return EXAM * discipline.BudgetStudents;
            }
        }
        public double ExamsOffBudget
        {
            get
            {
                return EXAM * discipline.OffBudgetStudents;
            }
        }

        public double AllHoursBudget
        {
            get
            {
                return ModuleRatingSystemBudget + CourseProjectsBudget
                    + ConsultationBudget + TestReviewBudget + OffsetsBudget
                    + ExamsBudget;
            }
        }
        public double AllHoursOffBudget
        {
            get
            {
                return ModuleRatingSystemOffBudget + CourseProjectsOffBudget
                    + ConsultationOffBudget + TestReviewOffBudget + OffsetsOffBudget
                    + ExamsOffBudget;
            }
        }





    }
}

