﻿using CodeFirstEF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentOutput
{
    class DepartmentState
    {
        public DepartmentState()
        {
            Rows = new List<DepartmentStateRow>();
        }
        public DepartmentState(List<Discipline> disciplines)
        {
            Rows = new List<DepartmentStateRow>();
            foreach (Discipline discipline in disciplines)
            {
                Rows.Add(new DepartmentStateRow(discipline));
            }
        }
        public List<DepartmentStateRow> Rows;
    }
}

