﻿using CodeFirstEF;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
namespace DocumentOutput
{
    public static class StateMaker
    {
        private static string pathToRowTemplate = Environment.CurrentDirectory + @"\StateRowTemplate.xlsx";
        private static string pathToStateTemplate = Environment.CurrentDirectory + @"\StateTemplate.xlsx";

        private static Excel.Application app = new Excel.Application();
        private static Excel.Workbook workbookRowTemplate = app.Workbooks.Open(pathToRowTemplate);
        private static Excel.Worksheet worksheetRowTemplate = workbookRowTemplate.Worksheets.get_Item(1);
        private static Excel.Range rowTemplate = worksheetRowTemplate.Range["Data"];


        private static Excel.Workbook workbookHead = app.Workbooks.Open(pathToStateTemplate);
        private static Excel.Worksheet worksheetHead = workbookHead.Worksheets.get_Item(1);
        private static Excel.Range head = worksheetHead.Range["Head"];



        public static void MakeState(List<Discipline> disciplines)
        {
            Excel.Workbook state = app.Workbooks.Add();
            Excel.Worksheet stateWorksheet = state.Worksheets.get_Item(1);
            Format(stateWorksheet);
            CopyHeadFromTemplate(stateWorksheet);


            int order_number = 1;
            int rowNumber = 8;
            for (int i = 0; i < 5; i++)
            {
                DepartmentStateRow row = new DepartmentStateRow(disciplines[i]);
                WriteToTemplate(order_number, row);                
                WriteToState(stateWorksheet,rowNumber);
                Console.WriteLine(order_number + " discipline added to state...");
                order_number++;
                rowNumber += 2;
            }            
            state.Close(true);
            Finish();
        }

        private static void Format(Excel.Worksheet worksheet)
        {
            worksheet.StandardWidth = 4.5;
            worksheet.Columns[2].ColumnWidth = 17;
            worksheet.Rows[5].RowHeight = 100;
        }

        private static void WriteToState( Excel.Worksheet worksheet,int rowNumber)
        {
            rowTemplate.Copy();            
            Excel.Range pasterange = worksheet.Range["A" + rowNumber];
            pasterange.PasteSpecial();
        }

        private static void Finish()
        {
            ClearTemplate();
            workbookRowTemplate.Close(true);
            workbookHead.Close(false);
            app.Quit();
            CloseProcess();
        }

        private static void CopyHeadFromTemplate(Excel.Worksheet worksheet)
        {
            head.Copy();
            Excel.Range stateHead = worksheet.Range["A1"];
            stateHead.PasteSpecial();
        }

        private static void ClearTemplate()
        {
            for (int i = 1; i < rowTemplate.Count + 1; i++)
            {
                rowTemplate[i].Value = "";
            }
        }

        private static void WriteToTemplate(int number, DepartmentStateRow row)
        {

            Excel.Range range = worksheetRowTemplate.Range["Number"];
            range[1].Value = number.ToString();

            range = worksheetRowTemplate.Range["Title"];
            range[1].Value = row.discipline.Title;

            range = worksheetRowTemplate.Range["Course"];
            range[1].Value = row.discipline.Course.ToString();

            range = worksheetRowTemplate.Range["BudgetStudents"];
            range[1].Value = row.discipline.BudgetStudents.ToString();

            range = worksheetRowTemplate.Range["OffBudgetStudents"];
            range[1].Value = row.discipline.OffBudgetStudents.ToString();

            range = worksheetRowTemplate.Range["OffBudgetStreams"];
            range[1].Value = row.Streams.ToString();

            range = worksheetRowTemplate.Range["BudgetGroups"];
            range[1].Value = row.discipline.BudgetGroups.ToString();

            range = worksheetRowTemplate.Range["OffBudgetGroups"];
            range[1].Value = row.discipline.OffBudgetGroups.ToString();

            range = worksheetRowTemplate.Range["LectionsPlan"];
            range[1].Value = row.discipline.LectionHours.ToString();

            range = worksheetRowTemplate.Range["Lections"];
            range[1].Value = row.discipline.LectionHours.ToString();

            range = worksheetRowTemplate.Range["LabPlan"];
            range[1].Value = row.discipline.LectionHours.ToString();

            range = worksheetRowTemplate.Range["Lab"];
            range[1].Value = row.discipline.LectionHours.ToString();

            range = worksheetRowTemplate.Range["PracticlePlan"];
            range[1].Value = row.discipline.LectionHours.ToString();

            range = worksheetRowTemplate.Range["Practicle"];
            range[1].Value = row.discipline.LectionHours.ToString();

            range = worksheetRowTemplate.Range["ModuleRatingBudget"];
            range[1].Value = row.ModuleRatingSystemBudget.ToString();

            range = worksheetRowTemplate.Range["ModuleRatingOffBudget"];
            range[1].Value = row.ModuleRatingSystemOffBudget.ToString();

            range = worksheetRowTemplate.Range["CourseProjectsBudget"];
            range[1].Value = row.CourseProjectsBudget.ToString();

            range = worksheetRowTemplate.Range["CourseProjectsOffBudget"];
            range[1].Value = row.CourseProjectsOffBudget.ToString();

            range = worksheetRowTemplate.Range["ConsultationsBudget"];
            range[1].Value = row.ConsultationBudget.ToString();

            range = worksheetRowTemplate.Range["ConsultationsOffBudget"];
            range[1].Value = row.ConsultationOffBudget.ToString();

            range = worksheetRowTemplate.Range["TestReviewBudget"];
            range[1].Value = row.TestReviewBudget.ToString();

            range = worksheetRowTemplate.Range["TestReviewOffBudget"];
            range[1].Value = row.TestReviewOffBudget.ToString();

            range = worksheetRowTemplate.Range["OffsetsBudget"];
            range[1].Value = row.OffsetsBudget.ToString();

            range = worksheetRowTemplate.Range["OffsetsOffBudget"];
            range[1].Value = row.OffsetsOffBudget.ToString();

            range = worksheetRowTemplate.Range["ExamsBudget"];
            range[1].Value = row.ExamsBudget.ToString();

            range = worksheetRowTemplate.Range["ExamsOffBudget"];
            range[1].Value = row.ExamsOffBudget.ToString();

            range = worksheetRowTemplate.Range["ExamsBudget"];
            range[1].Value = row.ExamsBudget.ToString();

            range = worksheetRowTemplate.Range["ExamsOffBudget"];
            range[1].Value = row.ExamsOffBudget.ToString();
            
            range = worksheetRowTemplate.Range["AllHoursBudget"];
            range[1].Value = row.AllHoursBudget.ToString();

            range = worksheetRowTemplate.Range["AllHoursOffBudget"];
            range[1].Value = row.AllHoursOffBudget.ToString();

        }

        private static void CloseProcess()
        {
            Process[] List;
            List = Process.GetProcessesByName("EXCEL");
            foreach (Process proc in List)
            {
                proc.Kill();
            }
        }


    }
}
