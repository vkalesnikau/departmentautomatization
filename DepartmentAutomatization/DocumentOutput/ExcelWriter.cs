﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace DocumentOutput
{
    public static class ExcelWriter
    {
        public static void WriteToNamedRange(string pathToFile, string rangeName, string value)
        {

            Excel.Application app = new Excel.Application();
            try
            {
                Excel.Workbook workbook = app.Workbooks.Open(pathToFile);
                Excel.Worksheet worksheet = workbook.Worksheets.get_Item(1);
                Excel.Range range = worksheet.Range[rangeName];

                range[1].Value = value;
                app.Application.ActiveWorkbook.Save();
                workbook.Close(true);
            }
            finally
            {

                CloseProcess();
            }

        }
        private static void CloseProcess()
        {
            Process[] List;
            List = Process.GetProcessesByName("EXCEL");
            foreach (Process proc in List)
            {
                proc.Kill();
            }
        }

        public static void ClearRange(string pathToFile, string rangeName)
        {
            Excel.Application app = new Excel.Application();
            try
            {
                Excel.Workbook workbook = app.Workbooks.Open(pathToFile);
                Excel.Worksheet worksheet = workbook.Worksheets.get_Item(1);
                Excel.Range range = worksheet.Range[rangeName];
                for (int i = 1; i < range.Count + 1; i++)
                {
                    range[i].Value = "";
                }
                workbook.Close(true);
            }
            finally
            {
                CloseProcess();
            }
        }
        public static void CopyFromTemplateToState(int rowNumber)
        {
            string pathToRowTemplate = Environment.CurrentDirectory + @"\StateRowTemplate.xlsx";
            string pathToStateTemplate = Environment.CurrentDirectory + @"\StateTemplate.xlsx";

            Excel.Application app = new Excel.Application();
            try
            {
                Excel.Workbook workbookSrc = app.Workbooks.Open(pathToRowTemplate);
                Excel.Worksheet worksheetSrc = workbookSrc.Worksheets.get_Item(1);
                Excel.Range copyRnage = worksheetSrc.Range["Data"];
                copyRnage.Copy();

                Excel.Workbook workbookDst = app.Workbooks.Open(pathToStateTemplate);
                Excel.Worksheet worksheetDst = workbookDst.Worksheets.get_Item(1);
                Excel.Range pasterange = worksheetDst.Range["A"+rowNumber];
                pasterange.PasteSpecial();

                workbookSrc.Close(false);
                //workbookDst.SaveAs("State" + DateTime.UtcNow);
                workbookDst.Close(true);
            }
            finally
            {
                CloseProcess();
            }
        }

    }
}
