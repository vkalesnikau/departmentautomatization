﻿using DepartmentAutomatizationConsole;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DepartmentAutomatizationWindowsForms
{
    public partial class DatabaseForm : Form
    {
        private Form1 StateForm;

        public DatabaseForm()
        {
            InitializeComponent();
        }
        public DatabaseForm(Form1 stateForm)
        {
            InitializeComponent();
            StateForm = stateForm;
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            textBox1.Text = openFileDialog1.FileName;
            pathToPlan = openFileDialog1.FileName;
        }

        private string pathToPlan { get; set; }
        private string pathToContingent { get; set; }

    private void button1_Click(object sender, EventArgs e)
        {
            
            Writer.WriteInfoToDatabase(pathToPlan, pathToContingent);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            openFileDialog2.ShowDialog();
        }

        private void openFileDialog2_FileOk(object sender, CancelEventArgs e)
        {
            textBox2.Text = openFileDialog2.FileName;
            pathToContingent = openFileDialog2.FileName;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Writer.ClearDatabase();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            StateForm.Visible = true;
        }
    }
}
