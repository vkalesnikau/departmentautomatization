﻿using CodeFirstEF;
using CodeFirstEF.Repositories;
using DocumentOutput;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DepartmentAutomatizationWindowsForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DisciplineRepository discRepo = new DisciplineRepository();
            List<Discipline> disciplines = discRepo.GetAll();
            StateMaker.MakeState(disciplines);
        }

        
        private void button2_Click(object sender, EventArgs e)
        {
            DatabaseForm databaseForm = new DatabaseForm(this);
            databaseForm.ShowDialog();
            this.Visible=false;
            
        }
    }
}
