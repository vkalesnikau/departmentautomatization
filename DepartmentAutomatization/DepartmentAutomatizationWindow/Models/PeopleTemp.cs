﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DepartmentAutomatizationWindow.Models
{
    [Serializable]
    public class PeopleTemp
    {
        public string Name { get; set; }
        public string Position { get; set; }
        public List<DisciplineTemp> Disciplines { get; set; } = new List<DisciplineTemp>();

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            return ((obj as PeopleTemp).Name == this.Name && (obj as PeopleTemp).Position == this.Position);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() + Position.GetHashCode();
        }

        public override string ToString()
        {
            return Name;
        }

        public PeopleTemp() { }
    }
}
