﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DepartmentAutomatizationWindow.Models
{
    [Serializable]
    public class PlanPeopleTemp
    {
        public PeopleTemp Lecture { get; set; }
        public PeopleTemp Labs { get; set; }
        public PeopleTemp Practice { get; set; }
    }
}