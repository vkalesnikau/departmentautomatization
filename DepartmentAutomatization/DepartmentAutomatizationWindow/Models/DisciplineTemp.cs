﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DepartmentAutomatizationWindow.Models
{
    [Serializable]
    public class DisciplineTemp
    {
        public PlanPeopleTemp[] BudgetPeoples { get; set; }
        public PlanPeopleTemp[] FeePeoples { get; set; }

        public int BudgetCount { get; set; }
        public int FeeCount { get; set; }

        public string Name { get; set; }
        public int Course { get; set; }

        public SemesterTemp? Semester { get; set; }
        public StageTemp? Stage { get; set; }
        public DepartmentTemp? Department { get; set; }

        public PlanTemp Plan { get; set; }
        public PlanTemp ForPeople { get; set; }

        public double Sum { get { return Plan.Sum; } }
        public List<double> Hours { get; set; } = new List<double>();

        public override string ToString()
        {
            return Name;
        }

        public DisciplineTemp() { }


        public static bool operator ==(DisciplineTemp left, DisciplineTemp right)
        {
            return (left.Course == right.Course && left.Name == right.Name && left.Semester == right.Semester);
        }

        public static bool operator !=(DisciplineTemp left, DisciplineTemp right)
        {
            return !(left == right);
        }
    }
}
