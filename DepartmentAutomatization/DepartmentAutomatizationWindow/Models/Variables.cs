﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DepartmentAutomatizationWindow.Models
{
    public class Variables : OnPropertyChangedClass
    {
        private List<DisciplineTemp> globalDisciplinesRB;
        private List<PeopleTemp> globalPeoplesRB;
        private List<DisciplineTemp> globalDisciplinesRF;
        private List<PeopleTemp> globalPeoplesRF;

        private bool loadedDisciplinesRB;
        private bool loadedPeoplesRB;
        private bool loadedDisciplinesRF;
        private bool loadedPeoplesRF;



        public bool LoadedDisciplinesRB
        {
            get => loadedDisciplinesRB;
            set
            {
                loadedDisciplinesRB = value;
                OnPropertyChanged();
            }
        }

        public bool LoadedPeoplesRB
        {
            get => loadedPeoplesRB;
            set
            {
                loadedPeoplesRB = value;
                OnPropertyChanged();
            }
        }

        public bool LoadedDisciplinesRF
        {
            get => loadedDisciplinesRF;
            set
            {
                loadedDisciplinesRF = value;
                OnPropertyChanged();
            }
        }
        public bool LoadedPeoplesRF
        {
            get => loadedPeoplesRF;
            set
            {
                loadedPeoplesRF = value;
                OnPropertyChanged();
            }
        }


        public List<DisciplineTemp> GlobalDisciplinesRB
        {
            get => globalDisciplinesRB;
            set
            {
                globalDisciplinesRB = value;
                LoadedDisciplinesRB = true;
                OnPropertyChanged();
            }
        }

        public List<PeopleTemp> GlobalPeoplesRB
        {
            get => globalPeoplesRB;
            set
            {
                globalPeoplesRB = value;
                LoadedPeoplesRB = true;

                OnPropertyChanged();
            }
        }

        public List<DisciplineTemp> GlobalDisciplinesRF
        {
            get => globalDisciplinesRF;
            set
            {
                globalDisciplinesRF = value;
                LoadedDisciplinesRF = true;
                OnPropertyChanged();
            }
        }

        public List<PeopleTemp> GlobalPeoplesRF
        {
            get => globalPeoplesRF;
            set
            {
                globalPeoplesRF = value;
                LoadedPeoplesRF = true;
                OnPropertyChanged();
            }
        }
    }
}
