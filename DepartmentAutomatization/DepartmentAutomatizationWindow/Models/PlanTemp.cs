﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DepartmentAutomatizationWindow.Models
{
    [Serializable]
    public class PlanTemp
    {
        public double Lectures { get; set; }
        public double Labs { get; set; }
        public double Practices { get; set; }
        public double Sum { get { return Lectures + Labs + Practices; } }


        public PlanTemp() { }

        public PlanTemp(double Lectures, double Labs, double Practices)
        {
            this.Lectures = Lectures;
            this.Labs = Labs;
            this.Practices = Practices;
        }
    }
}
