﻿using DepartmentAutomatizationWindow.Logics;
using DepartmentAutomatizationWindow.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using WinForms = System.Windows.Forms;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;

namespace DepartmentAutomatizationWindow.Pages
{
    /// <summary>
    /// Логика взаимодействия для Plan.xaml
    /// </summary>
    public partial class Plan : Grid
    {
        Variables variables;

        public Plan(Variables variable)
        {
            this.variables = variable;
            InitializeComponent();
        }

        private void Save()
        {
            SavePeoples.Save(variables.GlobalPeoplesRB, Path.Text);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Save();
        }

        private void SaveAs(object sender, RoutedEventArgs e)
        {
            WinForms.FolderBrowserDialog fbd = new WinForms.FolderBrowserDialog { Description = "^_^", SelectedPath = $"{Path.Text}" };
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                Path.Text = fbd.SelectedPath;
        }

    }
}
