﻿using CodeFirstEF;
using CodeFirstEF.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DepartmentAutomatizationWindow.Pages
{
    /// <summary>
    /// Логика взаимодействия для Distribution.xaml
    /// </summary>
    public partial class Distribution : Grid
    {
        DisciplineRepository disciplineRepo;
        List<string> Budgets = new List<string> { "Республика Беларусь", "Российская Федерация"};
        List<string> Forms = new List<string> { "Дневная", "Заочная" };
        List<string> Degrees = new List<string> { "Бакалавриат", "Магистратура" };
        List<string> Semesters = new List<string> { "Весенний", "Осенний" };
        List<string> Teachers = new List<string>
        {
            "Широченко Виктор Александрович",
            "Пузанова Татьяна Владимировна",
            "Ливинская Виктория Александровна",
            "Токменинов Константин Александрович",
            "Пичугова Ольга Анатольевна",
            "Жесткова Елена Сергеевна",
            "Лобанова Татьяна Михайловна",
            "Ращеня Татьяна Федоровна",
            "Галкина Елена Геннадьевна",
            "Пеклина Ольга Валерьевна"
        };

        public Distribution()
        {
            InitializeComponent();
            disciplineRepo = new DisciplineRepository();
            FillComboBoxes();
        }

        private void FillComboBoxes()
        {
            Budgets.ForEach(e => BudgerChoose.Items.Add(e));
            Forms.ForEach(e => FormChoose.Items.Add(e));
            Semesters.ForEach(e => SemesterChoose.Items.Add(e));
            Degrees.ForEach(e => DegreeChoose.Items.Add(e));
            Teachers.ForEach(e => TeacherChoose.Items.Add(e));
            FillDisciplines();
        }

        private void FillDisciplines()
        {
            List<Discipline> disciplines = disciplineRepo.GetAll();
            DisciplineChoose.ItemsSource = disciplines;
        }

        private void DisciplineChoose_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Discipline dis = (Discipline)DisciplineChoose.SelectedItem;
            Lection.Text = Convert.ToString(dis.LectionHours);
            Practic.Text = Convert.ToString(dis.PracticleHours);
            Lab.Text = Convert.ToString(dis.LabHours);
            Supervised.Text = Convert.ToString(dis.SupervisedIndependentWorks);
            CourseWork.Text = Convert.ToString(dis.CourseWorks);
            Offset.Text = Convert.ToString(dis.Offsets);
            Exam.Text = Convert.ToString(dis.Exams);


        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
