﻿using ClosedXML.Excel;
using DepartmentAutomatizationWindow.Logics;
using DepartmentAutomatizationWindow.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using WinForms = System.Windows.Forms;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DepartmentAutomatizationWindow.Pages
{
    /// <summary>
    /// Логика взаимодействия для Information.xaml
    /// </summary>
    public partial class Information : Grid
    {
        Variables variables;

        public Information(Variables variables)
        {
            this.variables = variables;
            InitializeComponent();
            Semester.ItemsSource = new string[] { "Осень", "Весна" };
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Func<DisciplineTemp, bool> sort = d => d.Department == DepartmentTemp.Daytime && d.Semester == (SemesterTemp)(Semester.SelectedItem.Equals("Осень") ? 0 : 1) && d.Sum != 0;

            SaveInformation.Save(HelperDisciplines.FillWithPeoples(variables.GlobalDisciplinesRB.Where(sort).ToList(), variables.GlobalPeoplesRB).GroupBy(d => d.Stage).
                Concat(HelperDisciplines.FillWithPeoples(variables.GlobalDisciplinesRF.Where(sort).ToList(), variables.GlobalPeoplesRF).GroupBy(d => d.Stage)), Path.Text,
                Semester.SelectedItem.ToString());
        }

        private void SaveAs(object sender, RoutedEventArgs e)
        {
            WinForms.FolderBrowserDialog fbd = new WinForms.FolderBrowserDialog { Description = "^_^", SelectedPath = $"{Path.Text}" };
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                Path.Text = fbd.SelectedPath;
        }
    }
}
