﻿using DepartmentAutomatizationWindow.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DepartmentAutomatizationWindow.Pages
{
    /// <summary>
    /// Логика взаимодействия для Peoples.xaml
    /// </summary>
    public partial class Peoples : Grid
    {
        Variables variables;
        public Variables Variables { get { return variables; } }

        bool working;

        string currentPeople;

        public List<PeopleTemp> GetPeoples { get { return variables.GlobalPeoplesRB; } }

        Dictionary<string, TableBooks> contents = new Dictionary<string, TableBooks>();

        public Peoples(Variables variables)
        {
            this.variables = variables;
            InitializeComponent();
            this.DataContext = this;
        }

        private void RowSelected(object sender, RoutedEventArgs e)
        {
            if (((PeopleTemp)((DataGrid)sender).SelectedItem).Name == currentPeople)
                return;
            OpenBooks(((PeopleTemp)((DataGrid)sender).SelectedItem).Name);
        }


        private async Task OpenBooks(string name)
        {
            currentPeople = name;
            if (!contents.ContainsKey(currentPeople))
            {
                contents.Add(name, new TableBooks(name));
            }

            BooksOfPeople.Content = contents[name];
        }
    }
}
