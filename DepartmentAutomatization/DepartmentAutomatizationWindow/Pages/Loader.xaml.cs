﻿using DepartmentAutomatizationWindow.Logics;
using DepartmentAutomatizationWindow.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using WinForms = System.Windows.Forms;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;

namespace DepartmentAutomatizationWindow.Pages
{
    /// <summary>
    /// Логика взаимодействия для Plan.xaml
    /// </summary>
    public partial class Loader : Grid
    {
        Variables variables;
        public Variables Variables => variables;


        public Loader(Variables variables)
        {
            this.variables = variables;
            InitializeComponent();
        }

        private void SaveData(object sender, RoutedEventArgs e)
        {
            Dat.SaveAll(variables);
        }

        private void LoadFromDat(object sender, RoutedEventArgs e)
        {
            Dat.LoadAll(variables);
        }

        private void LoadFromExcel(object sender, RoutedEventArgs e)
        {
            if ((bool)CRB.IsChecked)
                variables.GlobalDisciplinesRB = HelperDisciplines.Get(Path.Text, "ПоСеместрам РБ");

            if ((bool)RRB.IsChecked)
                variables.GlobalPeoplesRB = HelperPeoples.Get(Path.Text, "Распределение РБ");

            if ((bool)CRF.IsChecked)
                variables.GlobalDisciplinesRF = HelperDisciplines.Get(Path.Text, "ПоСеместрам РФ");

            if ((bool)RRF.IsChecked)
                variables.GlobalPeoplesRF = HelperPeoples.Get(Path.Text, "Распределение РФ");
        }

        private void SaveAs(object sender, RoutedEventArgs e)
        {
            WinForms.OpenFileDialog fbd = new WinForms.OpenFileDialog() { Filter = "EXCEL (*.xlsx)|*.xlsx" };
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                Path.Text = fbd.FileName;
        }

    }
}
