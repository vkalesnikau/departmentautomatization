﻿using DepartmentAutomatizationWindow.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AngleSharp.Html.Parser;
using System.Net;
using System.IO;

namespace DepartmentAutomatizationWindow.Pages
{
    /// <summary>
    /// Логика взаимодействия для Books.xaml
    /// </summary>
    public partial class TableBooks : Grid
    {
        private List<Book> books;
        public List<Book> Books { get { return books; } }

        public TableBooks(string name)
        {
            books = ParseHtml(Request(name));
            InitializeComponent();
            this.DataContext = this;
        }

        private string Request(string name)
        {
            string correctName = name.Replace(" ", ", ").Replace(".", ". ");
            string data = $"rpp=10000&page=0&filtertype_0=author&filter_relational_operator_0=equals&filter_0={correctName}";

            WebRequest request = WebRequest.Create("http://e.biblio.bru.by/discover?" + data);
            WebResponse response = request.GetResponse();
            string ret;

            using (Stream stream = response.GetResponseStream())
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    ret = reader.ReadToEnd();
                }
            }
            return ret;
        }

        private List<Book> ParseHtml(string html)
        {
            HtmlParser htmlParser = new HtmlParser();
            var document = htmlParser.ParseDocument(html);
            var items = document.QuerySelector(".ds-artifact-list").GetElementsByClassName("artifact-description");

            List<Book> books = new List<Book>();
            foreach (var item in items)
            {
                books.Add(new Book
                {
                    Title = item.QuerySelector("a").TextContent,
                    Href = "http://e.biblio.bru.by" + item.QuerySelector("a").GetAttribute("href"),
                    Date = item.QuerySelector(".date").TextContent
                });
            }
            return books;
        }

        public class Book
        {
            public string Title { get; set; }
            public string Href { get; set; }
            public string Date { get; set; }
        }
    }


}
