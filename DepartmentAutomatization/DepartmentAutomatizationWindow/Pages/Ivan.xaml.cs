﻿using DepartmentAutomatizationConsole;
using System.Windows;
using System.Windows.Controls;
using WinForms = System.Windows.Forms;

namespace DepartmentAutomatizationWindow.Pages
{
    /// <summary>
    /// Логика взаимодействия для Ivan.xaml
    /// </summary>
    public partial class Ivan : Grid
    {
        public Ivan()
        {
            InitializeComponent();
        }

        private void SelectPlanPath(object sender, RoutedEventArgs e)
        {
            WinForms.OpenFileDialog fbd = new WinForms.OpenFileDialog();
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                PlanPath.Text = fbd.FileName;
        }

        private void SelectContingentPath(object sender, RoutedEventArgs e)
        {
            WinForms.OpenFileDialog fbd = new WinForms.OpenFileDialog();
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                ContingentPath.Text = fbd.FileName;
        }

        private void SelectStatePath(object sender, RoutedEventArgs e)
        {
            WinForms.OpenFileDialog fbd = new WinForms.OpenFileDialog();
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                StatePath.Text = fbd.FileName;
        }

        private void AddToDB(object sender, RoutedEventArgs e)
        {
            if (PlanPath.Text != "" && ContingentPath.Text != "")
                Writer.WriteInfoToDatabase(PlanPath.Text, ContingentPath.Text);
            System.Console.WriteLine("База заполнена");
        }

        private void ClearDB(object sender, RoutedEventArgs e)
        {
            Writer.ClearDatabase();
        }

        private void MakeState(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
