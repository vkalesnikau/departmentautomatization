﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace DepartmentAutomatizationWindow.Converters
{
    public class CurrentToColor : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return (System.Convert.ToInt32(values[0])) == System.Convert.ToInt32(values[1]) ? new SolidColorBrush(Color.FromRgb(175, 175, 175)) : Brushes.LightGray;
        }


        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
