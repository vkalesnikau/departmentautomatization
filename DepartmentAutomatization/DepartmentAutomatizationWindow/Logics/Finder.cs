﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DepartmentAutomatizationWindow.Logics
{
    public class Finder
    {
        public static List<IXLCell> Indexes(int column, IXLWorksheet worksheet)
        {
            List<IXLCell> indexes = new List<IXLCell>();

            worksheet.MergedRanges.Where(m => m.RangeAddress.FirstAddress.ColumnNumber == column &&
            m.RangeAddress.LastAddress.ColumnNumber == column &&
            m.RangeAddress.FirstAddress.RowNumber == m.RangeAddress.LastAddress.RowNumber - 1 &&
            worksheet.Cell(m.RangeAddress.FirstAddress).Value.ToString() != "" &&
            worksheet.Cell(m.RangeAddress.FirstAddress.RowNumber, m.RangeAddress.FirstAddress.ColumnNumber + 1).Value.ToString() != "").ToList().
            ForEach(e => indexes.Add(worksheet.Cell(e.RangeAddress.FirstAddress.RowNumber, e.RangeAddress.FirstAddress.ColumnNumber + 1)));
            return indexes;
        }

        public static List<IXLCell> Group(IXLWorksheet worksheet)
        {
            List<IXLCell> indexes = new List<IXLCell>();

            worksheet.MergedRanges.Where(m => m.RangeAddress.FirstAddress.ColumnNumber == 6 &&
            m.RangeAddress.LastAddress.ColumnNumber == 14 &&
            m.RangeAddress.FirstAddress.RowNumber == m.RangeAddress.LastAddress.RowNumber &&
            worksheet.Cell(m.RangeAddress.FirstAddress).Value.ToString() != "").OrderBy(e => e.RangeAddress.FirstAddress.RowNumber).ToList().
            ForEach(e => indexes.Add(worksheet.Cell(e.RangeAddress.FirstAddress)));
            return indexes;
        }
    }
}
