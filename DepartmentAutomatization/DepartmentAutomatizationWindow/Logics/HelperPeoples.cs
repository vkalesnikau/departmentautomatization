﻿using ClosedXML.Excel;
using DepartmentAutomatizationWindow.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DepartmentAutomatizationWindow.Logics
{
    public class HelperPeoples
    {
        public static List<PeopleTemp> Get(string path, string sheet)
        {
            List<PeopleTemp> peoples = new List<PeopleTemp>();

            using (var workbook = new XLWorkbook(path))
            {
                IXLWorksheet worksheet = workbook.Worksheet(sheet);
                List<IXLCell> indexes = Finder.Indexes(1, worksheet);
                indexes = indexes.ToList();
                indexes.ForEach(e => peoples.Add(Fill(e, worksheet)));
            }
            return peoples.Where(e => e.Disciplines.Count != 0).ToList();
        }

        private static PeopleTemp Fill(IXLCell index, IXLWorksheet worksheet)
        {
            int idMonth = 0;
            int start = index.Address.RowNumber + 2;
            PeopleTemp people = new PeopleTemp()
            {
                Name = worksheet.Cell(start - 2, 2).Value.ToString(),
                Position = worksheet.Cell(start - 2, 3).Value.ToString()
            };

            if (people.Position != "" && people.Position.Last() == '.')
                people.Position = people.Position.Substring(0, people.Position.Length - 1);

            DisciplineTemp temp;
            List<DisciplineTemp> disciplines = new List<DisciplineTemp>();

            while ((!worksheet.Cell(start, 2).IsMerged() && start <= 3000) || worksheet.Cell(start, 2).Value.ToString() == "Весна")
            {
                if (worksheet.Cell(start, 2).Value.ToString() == "Весна")
                {
                    idMonth = 1;
                    start++;
                    continue;
                }
                else if (worksheet.Cell(start, 2).Value.ToString() == "")
                {
                    start++;
                    continue;
                }
                temp = new DisciplineTemp() { Name = worksheet.Cell(start, 2).Value.ToString() };
                temp.Semester = idMonth == 0 ? SemesterTemp.Autumn : SemesterTemp.Spring;
                temp.Course = Convert.ToInt32(MyConverter.Convert(worksheet.Cell(start, 3).Value.ToString()));
                for (int column = 7; column < 22; column++)
                    temp.Hours.Add(MyConverter.Convert(worksheet.Cell(start, column).Value.ToString()));
                temp.ForPeople = new PlanTemp(MyConverter.Convert(worksheet.Cell(start, 7).Value), MyConverter.Convert(worksheet.Cell(start, 8).Value),
                                        MyConverter.Convert(worksheet.Cell(start, 9).Value));
                disciplines.Add(temp);
                start++;
            }
            people.Disciplines = disciplines;
            return people;
        }
    }
}
