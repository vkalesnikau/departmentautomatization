﻿using ClosedXML.Excel;
using DepartmentAutomatizationWindow.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DepartmentAutomatizationWindow.Logics
{
    public class HelperDisciplines
    {
        public static List<DisciplineTemp> Get(string path, string sheet)
        {
            List<DisciplineTemp> disciplines = new List<DisciplineTemp>();

            using (var workbook = new XLWorkbook(path))
            {
                var worksheet = workbook.Worksheet(sheet);

                List<IXLCell> cells = Finder.Group(worksheet);
                List<IXLCell> stages = cells.Where(e => e.Value.ToString().ToLower().Contains("студенты")).OrderBy(e => -e.Address.RowNumber).ToList();
                List<IXLCell> semesters = cells.Where(e => e.Value.ToString().ToLower().Contains("семестр")).OrderBy(e => -e.Address.RowNumber).ToList();

                Finder.Indexes(1, worksheet).ForEach(e => disciplines.Add(GetDiscipline(e.Address.RowNumber, worksheet, stages, semesters)));
            }
            return disciplines;
        }

        public static List<DisciplineTemp> FillWithPeoples(List<DisciplineTemp> disciplines, List<PeopleTemp> peoples)
        {
            List<PeopleTemp> peoplesForDiscipline;
            Dictionary<PeopleTemp, DisciplineTemp> Dictionary;
            foreach (DisciplineTemp discipline in disciplines)
            {
                peoplesForDiscipline = new List<PeopleTemp>();
                Dictionary = new Dictionary<PeopleTemp, DisciplineTemp>();
                foreach (PeopleTemp people in peoples)
                    foreach (DisciplineTemp disciplineOfPeople in people.Disciplines)
                        if (discipline == disciplineOfPeople)
                        {
                            peoplesForDiscipline.Add(people);
                            Dictionary.Add(people, disciplineOfPeople);
                        }

                for (int i = 0; i < discipline.BudgetCount; i++)
                {
                    discipline.BudgetPeoples[i].Lecture = peoplesForDiscipline.Find(e => discipline.Plan.Lectures != 0 && Dictionary[e].ForPeople.Lectures >= discipline.Plan.Lectures);
                }
                for (int i = 0; i < discipline.BudgetCount; i++)
                {
                    if (discipline.Plan.Labs != 0)
                    {
                        discipline.BudgetPeoples[i].Labs = peoplesForDiscipline.Find(e => discipline.Plan.Labs != 0 && Dictionary[e].ForPeople.Labs >= discipline.Plan.Labs);
                        if (discipline.BudgetPeoples[i].Labs != null)
                            Dictionary[discipline.BudgetPeoples[i].Labs].ForPeople.Labs -= discipline.Plan.Labs;
                    }
                    if (discipline.Plan.Practices != 0)
                    {
                        discipline.BudgetPeoples[i].Practice = peoplesForDiscipline.Find(e => discipline.Plan.Practices != 0 && Dictionary[e].ForPeople.Practices >= discipline.Plan.Practices);
                        if (discipline.BudgetPeoples[i].Practice != null)
                            Dictionary[discipline.BudgetPeoples[i].Practice].ForPeople.Practices -= discipline.Plan.Practices;
                    }
                }

                for (int i = 0; i < discipline.FeeCount; i++)
                {
                    if (discipline.Plan.Labs != 0)
                    {
                        discipline.FeePeoples[i].Labs = peoplesForDiscipline.Find(e => discipline.Plan.Labs != 0 && Dictionary[e].ForPeople.Labs >= discipline.Plan.Labs);
                        if (discipline.FeePeoples[i].Labs != null)
                            Dictionary[discipline.FeePeoples[i].Labs].ForPeople.Labs -= discipline.Plan.Labs;
                    }
                    if (discipline.Plan.Practices != 0)
                    {
                        discipline.FeePeoples[i].Practice = peoplesForDiscipline.Find(e => discipline.Plan.Practices != 0 && Dictionary[e].ForPeople.Practices >= discipline.Plan.Practices);
                        if (discipline.FeePeoples[i].Practice != null)
                            Dictionary[discipline.FeePeoples[i].Practice].ForPeople.Practices -= discipline.Plan.Practices;
                    }
                }
            }
            return disciplines;
        }

        private static DisciplineTemp GetDiscipline(int rowId, IXLWorksheet worksheet, List<IXLCell> stages, List<IXLCell> semesters)
        {
            StageTemp stage = StageTemp.First;
            DepartmentTemp department = DepartmentTemp.Extramural;
            SemesterTemp semester = SemesterTemp.Spring;
            foreach (IXLCell stageCell in stages)
                if (rowId > stageCell.Address.RowNumber)
                {
                    stage = stageCell.Value.ToString().ToLower().Contains("ii") ? StageTemp.Second : StageTemp.First;
                    break;
                }
            foreach (IXLCell semesterCell in semesters)
                if (rowId > semesterCell.Address.RowNumber)
                {
                    semester = semesterCell.Value.ToString().ToLower().Contains("осенний") ? SemesterTemp.Autumn : SemesterTemp.Spring;
                    department = semesterCell.Value.ToString().ToLower().Contains("дневное") ? DepartmentTemp.Daytime : DepartmentTemp.Extramural;
                    break;
                }

            int BudgetCount = Convert.ToInt32(MyConverter.Convert(worksheet.Cell(rowId, 6).CachedValue) * MyConverter.Convert(worksheet.Cell(rowId, 7).CachedValue));
            int FeeCount = Convert.ToInt32(MyConverter.Convert(worksheet.Cell(rowId + 1, 6).CachedValue) * MyConverter.Convert(worksheet.Cell(rowId + 1, 7).CachedValue));
            PlanPeopleTemp[] BudgetPeoples = new PlanPeopleTemp[BudgetCount];
            for (int i = 0; i < BudgetCount; i++)
                BudgetPeoples[i] = new PlanPeopleTemp();
            PlanPeopleTemp[] FeePeoples = new PlanPeopleTemp[FeeCount];
            for (int i = 0; i < FeeCount; i++)
                FeePeoples[i] = new PlanPeopleTemp();

            return new DisciplineTemp
            {
                BudgetPeoples = BudgetPeoples,
                FeePeoples = FeePeoples,
                BudgetCount = BudgetCount,
                FeeCount = FeeCount,
                Name = worksheet.Cell(rowId, 2).CachedValue.ToString(),
                Course = Convert.ToInt32(MyConverter.Convert(worksheet.Cell(rowId, 3).CachedValue)),
                Semester = semester,
                Department = department,
                Stage = stage,
                Plan = new PlanTemp
                {
                    Lectures = MyConverter.Convert(worksheet.Cell(rowId, 8).CachedValue),
                    Labs = MyConverter.Convert(worksheet.Cell(rowId, 10).CachedValue),
                    Practices = MyConverter.Convert(worksheet.Cell(rowId, 12).CachedValue)
                }
            };
        }
    }
}
