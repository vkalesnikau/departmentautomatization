﻿using ClosedXML.Excel;
using DepartmentAutomatizationWindow.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DepartmentAutomatizationWindow.Logics
{
    public class SavePeoples
    {
        public static void Save(List<PeopleTemp> peoples, string path)
        {
            using (XLWorkbook workbook = new XLWorkbook(@"шаблон.xlsx"))
            {
                IXLWorksheet worksheet = workbook.Worksheet("План");
                peoples.ForEach(people =>
                {
                    worksheet.Range(4, 3, 18, 22).Value = "";
                    worksheet.Range(21, 3, 35, 22).Value = "";

                    int row = 4;
                    people.Disciplines.Where(e => e.Semester == SemesterTemp.Autumn).ToList().ForEach(d =>
                    {
                        worksheet.Cell(row, 3).Value = d.Name;
                        for (int col = 0; col < d.Hours.Count(); col++)
                            worksheet.Cell(row, 8 + col).Value = d.Hours[col];
                        row++;
                    });
                    row = 21;
                    people.Disciplines.Where(e => e.Semester == SemesterTemp.Spring).ToList().ForEach(d =>
                    {
                        worksheet.Cell(row, 3).Value = d.Name;
                        for (int col = 0; col < d.Hours.Count(); col++)
                            worksheet.Cell(row, 8 + col).Value = d.Hours[col];
                        row++;
                    });
                    workbook.SaveAs($@"{path}\{people.Name}.xlsx");
                });
            }
        }
    }
}
