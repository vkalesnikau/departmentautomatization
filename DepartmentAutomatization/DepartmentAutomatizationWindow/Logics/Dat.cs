﻿using DepartmentAutomatizationWindow.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace DepartmentAutomatizationWindow.Logics
{
    public class Dat
    {
        public static void Save<T>(T obj, string name)
        {
            BinaryFormatter serializer = new BinaryFormatter();
            using (FileStream fs = new FileStream($@"{name}.dat", FileMode.OpenOrCreate))
            {
                serializer.Serialize(fs, obj);
            }
        }

        public static T Read<T>(string name)
        {
            BinaryFormatter serializer = new BinaryFormatter();
            using (FileStream fs = new FileStream($@"{name}.dat", FileMode.OpenOrCreate))
            {
                return (T)serializer.Deserialize(fs);
            }
        }

        public static void SaveAll(Variables variables)
        {
            Save(variables.GlobalDisciplinesRB, "disciplinesRB");
            Save(variables.GlobalPeoplesRB, "peoplesRB");
            Save(variables.GlobalDisciplinesRF, "disciplinesRF");
            Save(variables.GlobalPeoplesRF, "peoplesRF");
        }


        public static void LoadAll(Variables variables)
        {
            variables.GlobalDisciplinesRB = Read<List<DisciplineTemp>>("disciplinesRB");
            variables.GlobalPeoplesRB = Read<List<PeopleTemp>>("peoplesRB");

            variables.GlobalDisciplinesRF = Read<List<DisciplineTemp>>("disciplinesRF");
            variables.GlobalPeoplesRF = Read<List<PeopleTemp>>("peoplesRF");
        }
    }
}
