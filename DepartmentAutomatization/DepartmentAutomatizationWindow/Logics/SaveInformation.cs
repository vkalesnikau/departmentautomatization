﻿using ClosedXML.Excel;
using DepartmentAutomatizationWindow.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DepartmentAutomatizationWindow.Logics
{
    public class SaveInformation
    {
        public static void Save<T>(IEnumerable<IGrouping<T, DisciplineTemp>> groups, string path, string name)
        {
            using (XLWorkbook workbook = new XLWorkbook(@"шаблон сведения.xlsx"))
            {
                IXLWorksheet worksheet = workbook.Worksheet("Лист1");
                int rowId = 6;
                int number;

                foreach (IGrouping<T, DisciplineTemp> group in groups)
                {
                    number = 1;
                    foreach (IGrouping<int, DisciplineTemp> disciplines in group.OrderBy(e => e.Course).GroupBy(e => e.Course))
                    {
                        worksheet.Cell(rowId, 2).Value = $"Курс {disciplines.Key}";
                        rowId += 2;
                        foreach (DisciplineTemp discipline in disciplines)
                        {
                            worksheet.Cell(rowId, 1).Value = number++;
                            worksheet.Cell(rowId, 2).Value = discipline.Name;
                            if (discipline.BudgetPeoples != null && discipline.BudgetPeoples.Length != 0 && discipline.BudgetPeoples[0] != null &&
                                discipline.BudgetPeoples[0].Lecture != null)
                            {
                                worksheet.Cell(rowId, 4).Value = MyConverter.Around(discipline.Plan.Lectures, 17);
                                worksheet.Cell(rowId, 6).Value = $"{discipline.BudgetPeoples[0].Lecture.Position.ToLower()}, {discipline.BudgetPeoples[0].Lecture.Name}";
                            }
                            int labRow = 0, practriceRow = 0;

                            for (int i = 0; i < discipline.BudgetCount; i++)
                            {
                                if (discipline.BudgetPeoples[i].Labs != null)
                                {
                                    worksheet.Cell(rowId + labRow, 7).Value = MyConverter.Around(discipline.Plan.Labs, 17);
                                    worksheet.Cell(rowId + labRow++, 9).Value = $"{discipline.BudgetPeoples[i].Labs.Position.ToLower()}, {discipline.BudgetPeoples[i].Labs.Name}";
                                }
                                if (discipline.BudgetPeoples[i].Practice != null)
                                {
                                    worksheet.Cell(rowId + practriceRow, 10).Value = MyConverter.Around(discipline.Plan.Practices, 17);
                                    worksheet.Cell(rowId + practriceRow++, 12).Value = $"{discipline.BudgetPeoples[i].Practice.Position.ToLower()}, {discipline.BudgetPeoples[i].Practice.Name}";
                                }
                            }
                            for (int i = 0; i < discipline.FeeCount; i++)
                            {
                                if (discipline.FeePeoples[i].Labs != null)
                                {
                                    worksheet.Cell(rowId + labRow, 7).Value = MyConverter.Around(discipline.Plan.Labs, 17);
                                    worksheet.Cell(rowId + labRow++, 9).Value = $"{discipline.FeePeoples[i].Labs.Position.ToLower()}, {discipline.FeePeoples[i].Labs.Name}";
                                }
                                if (discipline.FeePeoples[i].Practice != null)
                                {
                                    worksheet.Cell(rowId + practriceRow, 10).Value = MyConverter.Around(discipline.Plan.Practices, 17);
                                    worksheet.Cell(rowId + practriceRow++, 12).Value = $"{discipline.FeePeoples[i].Practice.Position.ToLower()}, {discipline.FeePeoples[i].Practice.Name}";
                                }
                            }
                            for (int r = 0; r < (discipline.BudgetCount + discipline.FeeCount + 1) / 2; r++)
                                worksheet.Cell(rowId + r * 2, 3).Value = $"{disciplines.Key}-курс группа {r + 1}";

                            rowId += (discipline.BudgetCount + discipline.FeeCount + 1) / 2 * 2;
                        }
                    }
                    rowId += 2;
                }

                workbook.SaveAs($@"{path}\{name}.xlsx");
            }
        }
    }
}
