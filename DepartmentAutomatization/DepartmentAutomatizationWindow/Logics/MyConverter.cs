﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DepartmentAutomatizationWindow.Logics
{
    public class MyConverter
    {
        public static double Around(double value, double denominator)
        {
            value /= denominator;
            double f = value % 2;
            if (f > 1)
                f--;
            if (f > 0.75)
                value += 1;
            else if (f >= 0.25)
                value += 0.5;
            return value - f;
        }

        public static double Convert(object hours)
        {
            return Convert(hours.ToString());
        }

        public static double Convert(string hours)
        {
            return hours == "" ? 0 : System.Convert.ToDouble(hours.Replace(".", ","));
        }
    }
}
