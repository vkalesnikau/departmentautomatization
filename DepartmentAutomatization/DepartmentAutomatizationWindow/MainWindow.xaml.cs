﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DepartmentAutomatizationWindow.Models;
using DepartmentAutomatizationWindow.Pages;

namespace DepartmentAutomatizationWindow
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        Variables variables;

        Loader loader;
        Information information;
        Plan plan;
        Ivan ivan;
        Distribution distribution;
        Peoples peoples;

        public event PropertyChangedEventHandler PropertyChanged;

        int currentPage;
        public int CurrentPage
        {
            get => currentPage;
            set
            {
                currentPage = value;
                OnPropertyChanged();
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            variables = new Variables();

            ivan = new Ivan();
            distribution = new Distribution();

            plan = new Plan(variables);
            information = new Information(variables);
            loader = new Loader(variables);
            peoples = new Peoples(variables);


            Content.Content = distribution;
            CurrentPage = 4;
        }

        private void Plan_Click(object sender, RoutedEventArgs e)
        {
            Content.Content = plan;
            CurrentPage = 2;
        }
        
        private void Loader_Click(object sender, RoutedEventArgs e)
        {
            Content.Content = loader;
            CurrentPage = 0;
        }

        private void Information_Click(object sender, RoutedEventArgs e)
        {
            Content.Content = information;
            CurrentPage = 1;

        }
        private void Ivan_Click(object sender, RoutedEventArgs e)
        {
            Content.Content = ivan;
            CurrentPage = 3;
        }

        private void Distribution_Click(object sender, RoutedEventArgs e)
        {
            Content.Content = distribution;
            CurrentPage = 4;

        } 
        
        private void Peoples_Click(object sender, RoutedEventArgs e)
        {
            Content.Content = peoples;
            CurrentPage = 5;
        }

        public void OnPropertyChanged([CallerMemberName]string propertyName = "")
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        
    }
}
