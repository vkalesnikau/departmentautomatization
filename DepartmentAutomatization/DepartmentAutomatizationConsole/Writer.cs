﻿
using CodeFirstEF;
using CodeFirstEF.Repositories;
using ExcelManager;
using ExcelManager.Info;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DepartmentAutomatizationConsole
{
    public static class Writer
    {
        public static void WriteInfoToDatabase(string pathToPlan, string pathToContingent)
        {
            AcademicPlan academicPlan = new AcademicPlan(pathToPlan);
            Contingent contingent = new Contingent(pathToContingent);
            SpecialityRepository specRepo = new SpecialityRepository();
            DisciplineRepository discRepo = new DisciplineRepository();

            Speciality currentSpeciality = new Speciality(academicPlan.Speciality);
            
            ContingentInfo contingentInfo = contingent.GetContingentInfoBySpecialityTitle(currentSpeciality.Title);
            int counter = 1;
            foreach (DisciplineInfo disciplineInfo in academicPlan.Disciplines)
            {
                Discipline currentDisc = new Discipline(disciplineInfo, contingentInfo);
                discRepo.Insert(currentDisc);
                currentSpeciality.Disciplines.Add(currentDisc);
                Console.WriteLine(counter + "discipline added to database");
            }
            specRepo.Insert(currentSpeciality);
            Console.WriteLine("Data has been written to database!");
        }

        public static void ClearDatabase()
        {
            DisciplineRepository discRepo = new DisciplineRepository();
            SpecialityRepository specRepo = new SpecialityRepository();
            SemesterRepository semRepo = new SemesterRepository();
            semRepo.DeleteAll();
            discRepo.DeleteAll();
            specRepo.DeleteAll();
            Console.WriteLine("Dtabase is clear!");
        }

       
    }

}
