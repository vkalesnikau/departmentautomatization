﻿using CodeFirstEF;
using CodeFirstEF.Entities;
using CodeFirstEF.Repositories;
using DocumentOutput;
using ExcelManager;
using ExcelManager.Info;
using ExcelManager.Matchers;
using System;
using System.Collections.Generic;
using System.IO;

namespace DepartmentAutomatizationConsole
{
    public class Program
    {
        static void Main(string[] args)
        {
            string pathToPlan = Path.Combine(Environment.CurrentDirectory + "\\..\\..\\..\\..\\Excel\\Plan.xlsm");
            string pathToContingent = Path.Combine(Environment.CurrentDirectory + "\\..\\..\\..\\..\\Excel\\Kontingent.xlsx");

            //string pathToRowTemplate = Environment.CurrentDirectory + @"\StateRowTemplate.xlsx";
            //string pathToStateTemplate = Environment.CurrentDirectory + @"\StateTemplate.xlsx";

            Writer.ClearDatabase();
            //Writer.WriteInfoToDatabase(pathToPlan, pathToContingent);

            DisciplineRepository discRepo = new DisciplineRepository();
            List<Discipline> disciplines = discRepo.GetAll();
            Console.WriteLine(disciplines.Count+ " disciplines were recived from database...");
            StateMaker.MakeState(disciplines);
            //int number = 1;
            //int rowNumber = 8;
            //for (int i = 0; i < 20; i++)
            //{
            //    DepartmentStateRow row = new DepartmentStateRow(disciplines[i]);
            //    row.WriteToTemplate(number);
            //    Console.WriteLine(number +" discipline was written to template... ");
            //    DepartmentStateRow.WriteToState(rowNumber);
            //    Console.WriteLine(number+" discipline added to state...");
            //    number++;
            //    rowNumber += 2;
            //}
            //DepartmentStateRow.Finish();
            //foreach(Discipline discipline in disciplines)
            //{
            //    DepartmentStateRow row = new DepartmentStateRow(discipline);                
            //    row.WriteToTemplate(number);
            //    ExcelWriter.CopyFromTemplateToState(number+8);
            //    number++;

            //}

            //DepartmentStateRow row = new DepartmentStateRow(discRepo.GetItem(1046));
            //row.WriteToTemplate();            
            //ExcelWriter.CopyFromTemplateToState();

            // ExcelWriter.WriteToNamedRange(pathToRowTemplate, "Title", "Иностранный язык");
            // ExcelWriter.ClearRange(pathToRowTemplate, "Data");

            Console.WriteLine("Press any button...");
            Console.ReadKey();


        }

    }
}